package main

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"runtime"
	"strings"
	"time"

	vegeta "github.com/tsenart/vegeta/lib"
)

type task struct {
	Method      string
	URL         string
	RequestsNum uint64
	TaskRepeat  int
	Interval    time.Duration
	Timespan    time.Duration
	Payload     []byte
	Maxproc     int
	Output      string
	DirectOut   bool
	ContentType string
}

const (
	defaultTaskDuration   = "1m"
	defaultTimespan       = "1m"
	defaultRequestNumber  = 1
	defaultTaskNumber     = 1
	defaultMaxProcs       = 2
	defaultReportFileName = "reports"

	directOutput = iota + 1
	resumedOutput
)

func (t *task) start(reports []*report) {
	runtime.GOMAXPROCS(t.Maxproc)

	i := 0
	t.run(i, time.Now(), reports)

	for i < t.TaskRepeat {
		timer := time.NewTimer(t.Interval)
		<-timer.C
		t.run(i, time.Now(), reports)
		i++
	}
}

func (t *task) run(i int, now time.Time, reports []*report) {
	tskNum := i + 1
	log.Printf("Starting task #%d - %d requests for %s\n", tskNum, t.RequestsNum, t.Timespan)

	var metrics vegeta.Metrics
	var response string
	if t.DirectOut {
		response = t.makeDirect()
	} else {
		metrics = t.mkRequests(i)
	}

	reprt := &report{
		startTime: now,
		endTime:   time.Now(),
		Metrics:   metrics,
		direct:    response,
	}

	reports[i] = reprt

	log.Printf("Done task #%d", tskNum)
}

func (t *task) makeDirect() string {
	body := strings.NewReader(string(t.Payload))

	client := &http.Client{}
	req, err := http.NewRequest(t.Method, t.URL, body)
	if err != nil {
		return err.Error()
	}

	resp, err := client.Do(req)
	if err != nil {
		return err.Error()
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)

	return buf.String()
}

func (t *task) mkRequests(i int) vegeta.Metrics {
	rate := vegeta.Rate{Freq: int(t.RequestsNum), Per: t.Timespan}

	targeter := vegeta.NewStaticTargeter(vegeta.Target{
		Method: t.Method,
		URL:    t.URL,
		Body:   t.Payload,
		Header: http.Header{"Content-Type": {t.ContentType}},
	})
	attacker := vegeta.NewAttacker()

	var metrics vegeta.Metrics
	for res := range attacker.Attack(targeter, rate, t.Timespan, fmt.Sprintf("Task #%d", i)) {
		metrics.Add(res)
	}
	metrics.Close()

	return metrics
}

// SetPayloadFile reads the content of given file to sends as request payload
func (t *task) SetPayloadFile(f string) error {
	if f == "" {
		return nil
	}
	payload, err := ioutil.ReadFile(f)
	if err != nil {
		return errors.New("unable to handle payload file")
	}

	t.Payload = payload

	return nil
}
