package main

import (
	"fmt"
	"log"
	"time"

	"github.com/ogier/pflag"
)

const version = "0.0.3"

func main() {
	fmt.Println(fmt.Sprintf("Roboto %s", version))
	log.SetPrefix("[ ROBOTO ]")

	tsk := taskFromArgs()
	if tsk.URL == "" {
		pflag.Usage()
		return
	}

	var reports []*report
	reports = make([]*report, 1)

	if tsk.TaskRepeat > 0 {
		reports = make([]*report, tsk.TaskRepeat)
	}

	tsk.start(reports)
	produceOutput(reports, tsk.Output)
}

func taskFromArgs() (tsk *task) {
	tsk = &task{}
	var fname string

	pflag.StringVarP(&tsk.Method, "method", "x", "GET", "Request method [GET,POST,PUT,...]")
	pflag.StringVarP(&tsk.URL, "url", "u", "", "URL for requests")
	pflag.Uint64VarP(&tsk.RequestsNum, "reqnum", "n", 1, "Number of requests performed")
	pflag.IntVarP(&tsk.TaskRepeat, "repeat", "r", 1, "Number of task repetitions")
	pflag.DurationVarP(&tsk.Timespan, "time", "t", time.Minute, "Timespan for a single task execution")
	pflag.DurationVarP(&tsk.Interval, "interval", "i", time.Minute, "Interval between task executions")
	pflag.IntVar(&tsk.Maxproc, "cpu", 2, "Max CPU used for the task")
	pflag.StringVarP(&tsk.Output,
		"output", "o", "report", "Output file for report. If there is already a file with specified name, the result will be appended to it")
	pflag.StringVarP(&fname, "body", "b", "", "File containg the payload to be sent in the requests")
	pflag.BoolVarP(&tsk.DirectOut, "direct", "d", false, "Direct output for each request (writes the response body to report)")
	pflag.StringVarP(&tsk.ContentType, "type", "h", "application/json", "ContentType header for the requests")
	pflag.Parse()

	if err := tsk.SetPayloadFile(fname); err != nil {
		log.Fatalln(err)
	}

	return
}
