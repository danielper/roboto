package main

import (
	"fmt"
	"os"
	"time"

	vegeta "github.com/tsenart/vegeta/lib"
)

type report struct {
	startTime time.Time
	endTime   time.Time
	vegeta.Metrics
	direct string
}

const timeFormat = "2006-01-02 15:04:05"

func produceOutput(reports []*report, file string) {
	var f *os.File
	var err error
	if f, err = os.OpenFile(file, os.O_APPEND|os.O_WRONLY, 0644); err != nil {
		f, err = os.Create(file)
	}
	defer f.Close()

	f.Write([]byte("===== ROBOTO REPORT =====\n"))

	for i, r := range reports {
		taskTitle := fmt.Sprintf("\n=== Task #%d\n", i+1)
		f.Write([]byte(taskTitle))

		start := fmt.Sprintf("Started: \t%s\n", r.startTime.Format(timeFormat))
		finish := fmt.Sprintf("Finished: \t%s\n", r.endTime.Format(timeFormat))

		f.Write([]byte(start))
		f.Write([]byte(finish))

		if r.direct == "" {
			rp := vegeta.NewTextReporter(&r.Metrics)
			rp.Report(f)
		} else {
			f.Write([]byte(r.direct))
		}
	}

	f.Write([]byte("\n\n\n"))
}
